# Drone Dashboard

This project is the dashboard where drones can be monitored in real time. The project was started from scratch using the [create-react-app](https://loopback.io/) tool. [Here you can find the documentation](./readme_cra) for `create-react-app`.

### Usage

> npm run start

### Design decisions

- The communication with the back-end server is done via [Server Sent Events](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events)
- All events received from back-end alongside the data is sent to `DroneStore` component which applies the updates and thus obtains the correct state of each drone
- After each applied update, the `App`'s state is updated and the table is refreshed.
- The table used to display the drones is `react-table` component.

### Drone Statuses

Depending on the drone's status, a different color is used to display the text of the drone's status.

<span style="color:#ff0000">&#x25cf; Ready</span> -> displayed when the drone becomes idle (ie. the drone hanged in Ready state for more than 10 seconds)

<span style="color:#555">&#x25cf; Ready</span> - displayed when a drone is ready but has moved more than 1 meter in the last 10 seconds;

<span style="color:#ffbf00">&#x25cf; Recharging</span> -> Displayed when the drone is recharging

<span style="color:#57d500">&#x25cf; Flying</span> - Displayed when the drone is flying and thus moved more than 1 meter in the last 10 seconds;
