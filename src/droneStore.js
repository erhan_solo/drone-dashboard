export default class DroneStore {
  drones = [];

  handleDroneUpdate(type, du) {
    if (du.lastFlyTime && typeof du.lastFlyTime === "string") {
      du.lastFlyTime = new Date(du.lastFlyTime);
    }

    switch (type) {
      case "Add":
        this.drones.push({ ...du });
        break;
      case "Update":
        const droneIndex = this.drones.findIndex(d => d.uuid === du.uuid);
        const drone = this.drones[droneIndex];
        const updated = { ...drone, ...du };
        this.drones[droneIndex] = updated;
        break;
      default:
    }
  }
}
