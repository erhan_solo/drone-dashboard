import React, { Component } from "react";
import ReactTable from "react-table";
import { distanceInWordsStrict } from "date-fns";
import DroneStore from "./droneStore";

import "react-table/react-table.css";
import "./App.css";

const evtSourceAddr = "http://localhost:8080/drone/stream";
const neededDurationToIdle = 10000; // ms
const colorMapping = {
  Ready: "#555",
  Idle: "#ff0000",
  Flying: "#57d500",
  Recharging: "#ffbf00"
};

const columns = [
  {
    Header: "General Info",
    columns: [
      {
        Header: "Drone ID",
        id: "uuid",
        accessor: d => ({
          uuid: d.uuid,
          status: d.status,
          lastFlyTime: d.lastFlyTime
        }),
        sortMethod: (a, b) => {
          const uuidA = a.uuid.split("Drone_")[1];
          const uuidB = b.uuid.split("Drone_")[1];
          return parseInt(uuidA, 10) - parseInt(uuidB, 10);
        },
        Cell: row => {
          const { uuid, lastFlyTime, status } = row.value;
          const isIdle =
            status === "Ready" &&
            new Date() - lastFlyTime > neededDurationToIdle;

          return (
            <span style={{ color: isIdle ? colorMapping.Idle : "#000" }}>
              {uuid}
            </span>
          );
        }
      },
      {
        Header: "Status",
        id: "status",
        accessor: d => ({ status: d.status, lastFlyTime: d.lastFlyTime }),
        sortMethod: (a, b) => {
          const aStatus = a.status;
          const bStatus = b.status;
          if (aStatus === bStatus) {
            return b.lastFlyTime - a.lastFlyTime;
          }

          return aStatus.localeCompare(bStatus);
        },
        Cell: row => {
          const { status, lastFlyTime } = row.value;
          let color = colorMapping[status];
          let text = status;
          let isIdle = new Date() - lastFlyTime > neededDurationToIdle;
          if (isIdle && status === "Ready") {
            color = colorMapping.Idle;
          }

          return (
            <div style={{ width: "100%", textAlign: "left" }}>
              <span style={{ color: color }}>
                <span style={{ transition: "all .3s ease" }}>&#x25cf; </span>
                {text}
              </span>
            </div>
          );
        }
      },
      {
        Header: "Current Speed",
        id: "current_speed",
        accessor: d => {
          if (d.currentSpeed === 0) {
            return "-";
          }
          return ((d.currentSpeed * 3600) / 1000).toPrecision(3);
        },
        Cell: row => {
          return row.value > 0 ? <span>{row.value} km/h</span> : <span>-</span>;
        }
      }
    ]
  },
  {
    Header: "Current Location",
    columns: [
      {
        Header: "Latitude",
        id: "current_loc_latitude",
        accessor: d => d.currentLocation.lat.toPrecision(5)
      },
      {
        Header: "Longitude",
        id: "current_loc_longitude",
        accessor: d => d.currentLocation.lng.toPrecision(5)
      }
    ]
  },
  {
    Header: "Destination",
    headerClassName: "geo-location-header-group",
    columns: [
      {
        Header: "Latitude",
        id: "destination_latitude",
        accessor: d => (d.destination ? d.destination.lat.toPrecision(5) : "-")
      },
      {
        Header: "Longitude",
        id: "destination_longitude",
        accessor: d => (d.destination ? d.destination.lng.toPrecision(5) : "-")
      }
    ]
  }
];

class App extends Component {
  constructor(props) {
    super(props);
    this.droneStore = new DroneStore();
    this.init();
    this.state = {
      lastUpdated: new Date(),
      lastUpdatedFmt: distanceInWordsStrict(new Date(), new Date()),
      drones: []
    };
  }

  init() {
    const handler = ev => {
      let data;
      try {
        data = JSON.parse(ev.data);
      } catch (e) {
        console.error(e);
        return;
      }
      this.droneStore.handleDroneUpdate(ev.type, data);
      this.setState({
        lastUpdated: new Date(),
        drones: this.droneStore.drones.slice()
      });
    };

    this.evtSource = new EventSource(evtSourceAddr);

    this.evtSource.addEventListener("Add", handler);
    this.evtSource.addEventListener("Update", handler);

    this.evtSource.onerror = err => {
      console.error(err);
    };

    setInterval(() => {
      const { lastUpdated } = this.state;
      this.setState({
        lastUpdatedFmt: distanceInWordsStrict(lastUpdated, new Date())
      });
    }, 1000);
  }

  render() {
    const { drones } = this.state;

    return (
      <div className="App">
        <div className="App-header">
          <p className="App-intro">Last Updated: {this.state.lastUpdatedFmt}</p>
        </div>
        <div className="table-container">
          <ReactTable columns={columns} data={drones} />
        </div>
      </div>
    );
  }
}

export default App;
